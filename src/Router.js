import React from "react";
import { Scene, Router, Actions } from "react-native-router-flux";

import KimlikDogrulama from "./Components/KimlikDogrulama";
import StudentList from "./Components/StudentList";
import StudentCreate from "./Components/StudentCreate";
import StudentUpdate from "./Components/StudentUpdate";

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ marginTop: 55 }}>
      <Scene key="identity">
        <Scene key="login" component={KimlikDogrulama} title="Giriş Ekranı" />
      </Scene>
      <Scene key="main">
        <Scene
          key="studentList"
          component={StudentList}
          title="Giriş Ekranı"
          rightTitle="Yeni"
          onRight={() => {
            Actions.StudentCreate();
          }}
        />
        <Scene
          key="StudentCreate"
          component={StudentCreate}
          title="Kayıt Ekranı"
        />
        <Scene
          key="StudentUpdate"
          component={StudentUpdate}
          title="Öğrenci Güncelle"
        />
      </Scene>
    </Router>
  );
};
export default RouterComponent;
