import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import Reducers from "./Reducers";
import firebase from "firebase";
import Router from "./Router";
import ReduxThunk from "redux-thunk";
import logger from "redux-logger";

export default class Main extends Component {
  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyC6bMf-wcCqulnolL2I75EuXZDkcJwt8NU",
      authDomain: "reduxogreniyom.firebaseapp.com",
      databaseURL: "https://reduxogreniyom.firebaseio.com",
      projectId: "reduxogreniyom",
      storageBucket: "reduxogreniyom.appspot.com",
      messagingSenderId: "38949832464"
    });
  }

  render() {
    const store = createStore(
      Reducers,
      {},
      applyMiddleware(ReduxThunk, logger)
    );
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
