const initialstate = {
  isim: "",
  soyisim: "",
  ogrencinumara: "",
  sube: "",
  loading: false
};

export default (state = initialstate, action) => {
  switch (action.type) {
    case "student_changed":
      return { ...state, [action.payload.props]: action.payload.value };
    case "create_request":
      return { ...state, loading: true };
    case "create_request_success":
      return initialstate;

    default:
      return state;
  }
};
