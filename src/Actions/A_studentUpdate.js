import firebase from "firebase";
import { Actions } from "react-native-router-flux";

export const studentUpdate = ({ isim, soyisim, ogrencinumara, sube, uid }) => {
  const { currentUser } = firebase.auth();

  return dispatch => {
    dispatch({ type: "update_request" });

    firebase
      .database()
      .ref(`/kullanicilar/${currentUser.uid}/ogrenciler/${uid}`)
      .set({ isim, soyisim, ogrencinumara, sube })
      .then(() => {
        dispatch({ type: "update_request_success" });
        Actions.pop();
      });
  };
};

export const studentDelete = ({ uid }) => {
  const { currentUser } = firebase.auth();

  return dispatch => {
    dispatch({ type: "delete_request" });

    firebase
      .database()
      .ref(`/kullanicilar/${currentUser.uid}/ogrenciler/${uid}`)
      .remove()
      .then(() => {
        dispatch({ type: "delete_request_success" });
        Actions.pop();
      });
  };
};
