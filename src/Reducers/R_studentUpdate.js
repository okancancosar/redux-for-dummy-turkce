const initialstate = {
  loadingUpdate: false,
  loadingDelete: false
};

export default (state = initialstate, action) => {
  switch (action.type) {
    case "update_request":
      return { loadingUpdate: true };
    case "update_request_success":
      return initialstate;
    case "delete_request":
      return { loadingDelete: true };
    case "delete_request_success":
      return initialstate;
    default:
      return state;
  }
};
