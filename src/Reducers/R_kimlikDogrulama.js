const initialState = {
  email: "",
  password: "",
  loading: false,
  err: ""
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "email_changed":
      return { ...state, email: action.payload };
    case "password_changed":
      return { ...state, password: action.payload };
    case "login_user":
      return { ...state, loading: true };
    case "login_user_success":
      return { ...state, loading: false };
    case "login_user_error":
      return { ...state, loading: false, err: action.payload };
    default:
      return state;
  }
};
