import { Alert } from "react-native";
import firebase from "firebase";
import { Actions } from "react-native-router-flux";

export const emailChanged = text => {
  return dispatch => {
    dispatch({
      type: "email_changed",
      payload: text
    });
  };
};

export const passwordChanged = text => {
  return dispatch => {
    dispatch({
      type: "password_changed",
      payload: text
    });
  };
};

export const loginUser = ({ email, password }) => {
  return dispatch => {
    // Servise çıkmadan önce loading: true üretmek için bir dispatch oluştu.
    dispatch({ type: "login_user" });

    if (email === "" || password === "") {
      Alert.alert("Tüm alanları doldurun.");
    } else {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(user => loginSucces(dispatch, user))
        .catch(e => {
          // console.log("signIn " + e);
          firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(user => loginSucces(dispatch, user))
            .catch(err => {
              // console.log("createUser " + err);
              loginFail(dispatch, err);
            });
        });
    }
  };
};

const loginSucces = (dispatch, user) => {
  dispatch({
    type: "login_user_success",
    payload: user
  });
  Actions.main({ type: "reset" });
};

const loginFail = (dispatch, err) => {
  Alert.alert(err.code);
  dispatch({
    type: "login_user_error",
    payload: err
  });
};
