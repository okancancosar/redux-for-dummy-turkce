import { combineReducers } from "redux";
import R_kimlikDogrulama from "./R_kimlikDogrulama";
import R_studentCreate from "./R_studentCreate";
import R_studentList from "./R_studentList";
import R_studentUpdate from "./R_studentUpdate";

export default combineReducers({
  kimlikDogrulamaResponse: R_kimlikDogrulama,
  studentCreateResponse: R_studentCreate,
  studentListResponse: R_studentList,
  studentUpdateResponse: R_studentUpdate
});
