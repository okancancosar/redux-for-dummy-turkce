import React, { Component } from "react";
import { View, Text, StyleSheet, TextInput, Picker } from "react-native";
import { Button, Card, CardSection, Spinner } from "./ortak";
import { studentChange, studentCreate } from "../Actions";
import { connect } from "react-redux";

class StudentCreate extends Component {
  clickSave() {
    const { isim, soyisim, ogrencinumara, sube } = this.props;
    console.log(this.props);
    this.props.studentCreate({ isim, soyisim, ogrencinumara, sube });
  }
  renderButton() {
    if (!this.props.loading) {
      return <Button onPress={this.clickSave.bind(this)}> Kaydet </Button>;
    }
    return <Spinner size="small" />;
  }
  render() {
    const { inputStyle } = styles;
    return (
      <Card>
        <CardSection>
          <TextInput
            placeholder="İsim"
            style={inputStyle}
            value={this.props.isim}
            onChangeText={text => {
              this.props.studentChange({ props: "isim", value: text });
            }}
          />
        </CardSection>

        <CardSection>
          <TextInput
            placeholder="Soyisim"
            style={inputStyle}
            value={this.props.soyisim}
            onChangeText={text =>
              this.props.studentChange({ props: "soyisim", value: text })}
          />
        </CardSection>

        <CardSection>
          <TextInput
            placeholder="Öğrenci Numarası"
            style={inputStyle}
            value={this.props.ogrencinumara}
            onChangeText={text =>
              this.props.studentChange({ props: "ogrencinumara", value: text })}
          />
        </CardSection>

        <CardSection>
          <Text>Şube</Text>
          <Picker
            style={{ flex: 1 }}
            selectedValue={this.props.sube}
            onValueChange={text =>
              this.props.studentChange({ props: "sube", value: text })}
          >
            <Picker.Item label="A Şubesi" value="asube" />
            <Picker.Item label="B Şubesi" value="bsube" />
            <Picker.Item label="C Şubesi" value="csube" />
            <Picker.Item label="D Şubesi" value="dsube" />
          </Picker>
        </CardSection>

        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  inputStyle: {
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    flex: 1
  }
};
const mapToStateProps = ({studentCreateResponse}) => {
  const { isim, soyisim, ogrencinumara, sube, loading } = studentCreateResponse;
  console.log(isim);
  return { isim, soyisim, ogrencinumara, sube, loading };
};
export default connect(mapToStateProps, { studentChange, studentCreate })(
  StudentCreate
);
