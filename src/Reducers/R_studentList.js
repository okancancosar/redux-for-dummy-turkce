const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "student_list_success":
      // console.log(action.payload);
      return action.payload;
    default:
      return state;
  }
};
