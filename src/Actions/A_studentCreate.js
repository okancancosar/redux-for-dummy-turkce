import firebase from "firebase";
import { Actions } from "react-native-router-flux";

export const studentChange = ({ props, value }) => {
  return dispatch => {
    dispatch({
      type: "student_changed",
      payload: { props, value }
    });
  };
};

export const studentCreate = ({ isim, soyisim, ogrencinumara, sube }) => {
  const { currentUser } = firebase.auth();

  return dispatch => {
    dispatch({ type: "create_request" });

    firebase
      .database()
      .ref(`/kullanicilar/${currentUser.uid}/ogrenciler`)
      .push({ isim, soyisim, ogrencinumara, sube })
      .then(() => {
        dispatch({ type: "create_request_success" });
        Actions.pop();
      });
  };
};

export const studentsList = () => {
  const { currentUser } = firebase.auth();

  return dispatch => {
    firebase
      .database()
      .ref(`/kullanicilar/${currentUser.uid}/ogrenciler`)
      .on("value", snapshot => {
        dispatch({ type: "student_list_success", payload: snapshot.val() });
      });
  };
};
