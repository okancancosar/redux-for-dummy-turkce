import React, { Component } from "react";
import { View, TextInput } from "react-native";
import firebase from "firebase";
import { Button, Card, CardSection, Spinner } from "./ortak";
import { emailChanged, passwordChanged, loginUser } from "../Actions";
import { connect } from "react-redux";

class KimlikDogrulama extends Component {
  clickLogin() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password });
  }
  renderButton() {
    if (!this.props.loading) {
      return <Button onPress={this.clickLogin.bind(this)}> GİRİŞ </Button>;
    }
    return <Spinner size="small" />;
  }
  render() {
    const { inputStyle, mainview } = styles;
    return (
      <View style={mainview}>
        <Card>
          <CardSection>
            <TextInput
              placeholder="E-mail"
              style={inputStyle}
              value={this.props.email}
              onChangeText={email => this.props.emailChanged(email)}
            />
          </CardSection>

          <CardSection>
            <TextInput
              secureTextEntry
              placeholder="Şifre"
              style={inputStyle}
              value={this.props.password}
              onChangeText={password => this.props.passwordChanged(password)}
            />
          </CardSection>

          <CardSection>
            {this.renderButton()}
          </CardSection>
        </Card>
      </View>
    );
  }
}

const styles = {
  inputStyle: {
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    flex: 1
  },
  mainview: {
    flex: 1,
    backgroundColor: "white"
  }
};
const mapStateToProps = ({ kimlikDogrulamaResponse }) => {
  const { email, password, loading, err } = kimlikDogrulamaResponse;
  return {
    email: "test@test.com", // artık bu değişkenler this.props.email şeklinde kullanıabilir.
    password: "123456",
    loading,
    err
  };
};

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser
})(KimlikDogrulama);
